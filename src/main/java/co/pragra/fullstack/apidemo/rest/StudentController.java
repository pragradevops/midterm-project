package co.pragra.fullstack.apidemo.rest;

import co.pragra.fullstack.apidemo.domain.Student;
import co.pragra.fullstack.apidemo.services.StudentService;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;
import java.util.List;

@RestController
public class StudentController {

    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }


    @RequestMapping(path = "/api/students" , method = RequestMethod.GET, produces = "application/json" )
    public List<Student> getStudents(){
        return  this.studentService.getAllStudents();
    }

    @RequestMapping(path = "/api/student/{id}" , method = RequestMethod.GET, produces = "application/json" )
    public Student getStudentById(@PathVariable Long id){
        return  this.studentService.getStudentById(id);
    }

    @RequestMapping(path = "/api/student", method = RequestMethod.POST)
    public Student addStudent(@RequestBody  Student student){
       return this.studentService.addUpdateStudent(student);
    }
}
