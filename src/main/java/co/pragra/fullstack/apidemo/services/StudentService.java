package co.pragra.fullstack.apidemo.services;

import co.pragra.fullstack.apidemo.domain.Student;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Data
public class StudentService {

    List<Student> students;

    public StudentService() {
        this.students = new ArrayList<>();
        this.addNewStudent(new Student(1L,"Demo","Demo"));
        this.addNewStudent(new Student(2L,"Demo1","Demo1"));
    }

    public Student getStudentById(Long id){
        for (Student student : students){
            if(student.getId()==id){
                return student;
            }
        }
        return null;
    }

    public List<Student> getAllStudents(){
        return students;
    }

    public Student addNewStudent(Student student){
        this.students.add(student);
        return student;
    }
    
    public Student addUpdateStudent(Student theStudent){
    	
    	boolean existingStudent = false;
    	
        for (Student student : students){
            if(student.getId()==theStudent.getId()){
            	student.setId(theStudent.getId());
            	student.setFirstName(theStudent.getFirstName());
            	student.setLastName(theStudent.getLastName());
            	existingStudent = true;
                return theStudent;                
            }
        }
        if(!existingStudent){
            this.students.add(theStudent);
            return theStudent;
        }
        return null;
    }

}
